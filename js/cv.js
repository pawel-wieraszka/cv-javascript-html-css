var info =
    {
        "name": "Paweł",
        "surname": "Wieraszka",
        "adress1" : "Reformacka 19A/7",
        "adress2" : "80-808, Gdańsk"
    };

var educationS = [
    {
        "school": "VIII L.O. in Gdansk",
        "subject": "extended mathematics and physics",
        "years": "1999 - 2003",
        "title": ""

    },
    {
        "school": "Gdansk University of Technology",
        "subject": "Electrical engineering",
        "years": "2003 - 2008",
        "title": "Master of Science (2008)"
    },
    {
        "school": "Gdansk University of Technology",
        "subject": "Management and Economics",
        "years": "2007 - 2010",
        "title": "Master (2013)"
    },
    {
        "school": "Codementors",
        "subject": "Java Junior Developer",
        "years": "II.2018 - VI.2018",
        "title": ""
    }

];

var experienceS = [
    {
        "company": "SPIE Controlec Engineering",
        "adress": "Schiedam (Netherlands)",
        "years": "V.2010 - VIII.2013",
        "position": "Junior Engineer"
    },
    {
        "company": "Kalkuluj.pl Sp. z o.o.",
        "adress": "Gdansk",
        "years": "IX.2013 - now",
        "position": "Worker / Co-owner"
    }
];

var it_skills = [
    "Java", "JavaFX", "Maven", "Hibernate", "Java Persistence API", "SQL + MySQL", "NoSQL + MongoDB",
    "HTML", "CSS", "JavaScript", "jQuery", "JSON", "Angular", "Microservices", "Linux", "Git"
];

var lan_skills = [
    "English - Advanced", "Dutch - Advanced", "German - Basic"
];

var interests = [
    "speedway", "football", "sports overall", "astrology", "Rubik's Cube"
];

function populateCv(container) {

    var container = document.getElementById(container);
    article = document.createElement("article");
    container.appendChild(article);

    article.appendChild(createHeader("h2",'cv'));

    sectionInfo = document.createElement("section");
    article.appendChild(sectionInfo);

    var infoTable = createInfoTable(info);
    sectionInfo.appendChild(infoTable);

    sectionEdu = document.createElement("section");
    article.appendChild(sectionEdu);

    var eduTable = createEduTable('education', 'school', 'subject', 'title', 'years');
    sectionEdu.appendChild(eduTable);

    sectionExp = document.createElement("section");
    article.appendChild(sectionExp);

    var expTable = createExpTable('experience', 'company', 'adress', 'position', 'years');
    sectionExp.appendChild(expTable);

    sectionItSkills = document.createElement("section");
    article.appendChild(sectionItSkills);

    sectionItSkills.appendChild(createHeader("h3", 'it skills'));
    sectionItSkills.appendChild(createList(it_skills));

    sectionSkills = document.createElement("section");
    article.appendChild(sectionSkills);

    sectionSkills.appendChild(createHeader("h3", 'language skills'));
    sectionSkills.appendChild(createList(lan_skills));

    sectionInterests = document.createElement("section");
    article.appendChild(sectionInterests);

    sectionInterests.appendChild(createHeader("h3", 'interests'));
    sectionInterests.appendChild(createList(interests));
}

function createList(skills) {
    var ul = document.createElement("ul");

    for (var index in skills) {
        var skill = skills[index];
        var li = document.createElement("li");
        li.innerHTML = skill;
        ul.appendChild(li);
    }
    return ul;
}

function createExpTable(name, v1, v2, v3, v4) {
    var table = document.createElement("table");
    table.classList.add("Table");

    var th = document.createElement("th");
    th.innerHTML = name;
    th.colSpan = name.length;
    table.appendChild(th);

    table.appendChild(createExpRow(v1));
    table.appendChild(createExpRow(v2));
    table.appendChild(createExpRow(v3));
    table.appendChild(createExpRow(v4));

    return table;
}

function createExpRow(name) {
    var tr = document.createElement("tr");

    for (var index in experienceS) {
        var experience = experienceS[index];
        var td = document.createElement("td");
        td.innerHTML = experience[name];
        tr.appendChild(td);
    }

    return tr;
}

function createEduTable(name, v1, v2, v3, v4) {
    var table = document.createElement("table");
    table.classList.add("Table");

    var th = document.createElement("th");
    th.innerHTML = name;
    th.colSpan = name.length;
    table.appendChild(th);

    table.appendChild(createEduRow(v1));
    table.appendChild(createEduRow(v2));
    table.appendChild(createEduRow(v3));
    table.appendChild(createEduRow(v4));

    return table;
}

function createEduRow(name) {
    var tr = document.createElement("tr");

    for (var index in educationS) {
        var education = educationS[index];
        var td = document.createElement("td");
        td.innerHTML = education[name];
        tr.appendChild(td);
    }

    return tr;
}

function createInfoTable(info) {
    var table = document.createElement("table");
    table.classList.add("Table");

    var th = document.createElement("th");
    th.innerHTML = 'Basic information';
    th.colSpan = 2;
    table.appendChild(th);

    table.appendChild(createOneRow("Name:", info.name));
    table.appendChild(createOneRow("Surname:", info.surname));
    table.appendChild(createOneRow("Adress:", info.adress1));
    table.appendChild(createOneRow("", info.adress2));

    return table;
}

function createOneRow(header, value) {
    var tr = document.createElement("tr");

    var td1 = document.createElement("td");
    td1.innerHTML = header;
    tr.appendChild(td1);

    var td2 = document.createElement("td");
    td2.innerHTML = value;
    tr.appendChild(td2);

    return tr;
}

function createHeader(h, text) {
    var header = document.createElement("header");
    var h = document.createElement(h);
    h.innerHTML = text;
    header.appendChild(h);
    return header;
}